from django.conf.urls import patterns, include, url
from django.contrib import admin
from TodoDjangoApp import views

urlpatterns = patterns('',
    url(r'^persons/list/$', views.PersonListView, name='person_list_view'),
url(r'^person/(\d+)/$',views.PersonDetailView,name='person_detail_view'),
url(r'^person/form/$',views.postformupload, name='post_form_upload'),
url(r'^person/formset/$',views.personformupload, name='person_form_upload'),
)
