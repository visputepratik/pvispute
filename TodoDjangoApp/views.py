from django.shortcuts import render
from django.template import RequestContext
from django.shortcuts import render_to_response
from TodoDjangoApp.models import Person
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from TodoDjangoApp.forms import *
from django.forms.formsets import formset_factory

# Create your views here.
def PersonListView(request):
    # Request the context of the request.
    # The context contains information such as the client's machine details, for example.
    context = RequestContext(request)

    # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    persons = Person.objects.all()
    context_dict = {'persons': persons}
    

    # Return a rendered response to send to the client.
    # We make use of the shortcut function to make our lives easier.
    # Note that the first parameter is the template we wish to use.
    return render_to_response('index.html', context_dict, context)

def PersonDetailView(request,person_id):
    # Request the context of the request.
    # The context contains information such as the client's machine details, for example.
    context = RequestContext(request)

    # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    persons = Person.objects.all()
    person = persons.get(id=person_id)
    context_dict = {'p': person}
    

    # Return a rendered response to send to the client.
    # We make use of the shortcut function to make our lives easier.
    # Note that the first parameter is the template we wish to use.
    return render_to_response('index.html', context_dict, context)

def postformupload(request):
    if request.method == 'GET':
        form = PostForm()
    else:
        # A POST request: Handle Form Upload
        form = PostForm(request.POST) # Bind data from request.POST into a PostForm
 
        # If data is valid, proceeds to create a new post and redirect the user
        if form.is_valid():
            name = form.cleaned_data['name']
            age = form.cleaned_data['age']
            person = Person.objects.create(name=name,
                                         age=age)
            return HttpResponseRedirect(reverse('person_list_view'
                                                ))
 
    return render(request, 'form_upload.html', {
        'form': form,
    })

def personformupload(request):
    PersonFormSet = formset_factory(PersonForm,extra=5)
    if request.method == 'GET':
        formset = PersonForm()
    else:
        # A POST request: Handle Form Upload
        formset = PersonFormSet(request.POST) # Bind data from request.POST into a PostForm
 
        # If data is valid, proceeds to create a new post and redirect the user
        if formset.is_valid():
            for form in formset:
                form.save()
                # tname = form.clean_data['fname']
                # tage = form.clean_data['fage']
                # person = Person.objects.create(name=tname,age=tage)
                
            # person.save()

        return HttpResponseRedirect(reverse('person_list_view'
                                                ))
 
    return render(request, 'formset_upload.html', {
        'formset': PersonFormSet(),
    })
